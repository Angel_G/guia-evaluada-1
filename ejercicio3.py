
class proteina():

    def __init__(self, codigo,nombre,autor,clasificacion, expressionSystem, mutaciones,organismo):
        self.codigo = codigo
        self.nombre = nombre
        self.autor = autor
        self.clasificacion = clasificacion
        self.organismo = organismo
        self.expressionSystem = expressionSystem
        self.mutaciones = mutaciones


    def mostrar(self):
        print("Codigo: ", self.codigo)
        print("Nombre: ", self.nombre)
        print("autor: ", self.autor)
        print("clasificacion: ", self.clasificacion)
        print("organismo: ", self.organismo)
        print("expression system: ", self.expressionSystem)
        print("mutaciones: ", self.mutaciones)

    
proteina = proteina("7U9H",
                    "Escherichia Coli apo",
                    "Donkor, A.K. Musayev, F.N., Safo, M.K.",
                    "proteina de transporte",
                    "Escherichia Coli",
                    "No",
                    "Escherichia Coli")

proteina.mostrar()