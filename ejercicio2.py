from os import system
system("clear")


class Vacuna():
    def __init__(self, nombre, laboratorio):
        self.nombre = nombre
        self.laboratorio = laboratorio
        self.efectos = []

    def setEfectosSecundarios (self):
        ciclo = True
        
        while ciclo:
            print("Vacuna:  ", self.nombre)
            self.efectos.append(input("Efecto a añadir: "))
            decidir = input("si quiere agregar otro efecto presione cualquier tecla. Para salir use X:    >")
            if decidir == "X" or decidir == "x":
                print("Pasando a la siguiente vacuna")
                break
            

    def mostrarEfectos(self):
        print("Vacuna: ", self.nombre)
        print()
        for i in self.efectos:
            print("-", i)


vacuna1 = Vacuna("ala", "icb")
vacuna1.setEfectosSecundarios()


vacuna2 = Vacuna("planB", "rsu")
vacuna2.setEfectosSecundarios()


vacuna3 = Vacuna("coronavac", "5G")
vacuna3.setEfectosSecundarios()

vacuna1.mostrarEfectos()
vacuna2.mostrarEfectos()
vacuna3.mostrarEfectos()


