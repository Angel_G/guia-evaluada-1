class Planeta():
    def __init__(self, masa, densidad, diametro, distanciaSol, identificador, nombre):
        self.masa = masa
        self.densidad = densidad
        self.diametro = diametro
        self.distanciaSol = distanciaSol
        self.id = identificador
        self.nombre = nombre
        

class Sistema():
    def __init__(self, nombre):
        self.nombre = nombre
        self.planetas = []


    def anadirPlaneta(self, planeta):
        self.planetas.append(planeta)

    def consultarInfo(self):
        planeta = int(input("¿que planeta desea ver?"))
        print("Planeta: ", self.planetas[planeta].nombre)
        print("id: ", self.planetas[planeta].id)
        print("Masa: ", self.planetas[planeta].masa)
        print("Densidad", self.planetas[planeta].densidad)
        print("Diametro: ", self.planetas[planeta].diametro)
        print("Distancia al sol: ", self.planetas[planeta].distanciaSol)


planeta1 = Planeta(12, 1, 1, 1, 1, "Mercurio")
planeta2 = Planeta(14,5, 38, 76, 2, "venus")
planeta3 = Planeta(13,43,66, 789, 3, "tierra") 
sistema = Sistema("Solar")
sistema.anadirPlaneta(planeta1)
sistema.anadirPlaneta(planeta2)
sistema.anadirPlaneta(planeta3)
sistema.consultarInfo()

